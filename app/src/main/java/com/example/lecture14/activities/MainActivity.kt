package com.example.lecture14.activities

import android.accounts.AuthenticatorDescription
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.lecture14.R
import com.example.lecture14.activities.adapters.Adapter
import com.example.lecture14.activities.models.Mymodel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var ContentList= mutableListOf<Mymodel>()
    lateinit var adapter:Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        SetUp()
    }

     fun SetUp(){

        adapter= Adapter(ContentList)
         mainrecacle.layoutManager=GridLayoutManager(this,2)
         mainrecacle.adapter=adapter
         setdate()

         refresh.setOnRefreshListener {
             refresh.isRefreshing=true
             ContentList.clear()
             adapter.notifyDataSetChanged()
             Handler().postDelayed({
                 refresh.isRefreshing=false
                 setdate()
                 adapter.notifyDataSetChanged()
             },1000)

         }

    }





    private  fun setdate(){

        val randomContext= mutableListOf<Mymodel>()

        var model1=Mymodel(R.mipmap.kotlin,"Kotlin","programing language")
        val model2=Mymodel(R.mipmap.c,"C","programing language")
        val model3=Mymodel(R.mipmap.python,"Python","programing language")
        val model4=Mymodel(R.mipmap.swift,"Swift","programing language")
        val model5=Mymodel(R.mipmap.ruby,"Ruby","programing language")

        randomContext.add(model1)
        randomContext.add(model2)
        randomContext.add(model3)
        randomContext.add(model4)
        randomContext.add(model5)


        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])
        ContentList.add(randomContext[(randomContext.indices).random()])


    }
}
