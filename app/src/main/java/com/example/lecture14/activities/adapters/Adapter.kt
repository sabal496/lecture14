package com.example.lecture14.activities.adapters

import android.view.LayoutInflater
import android.view.LayoutInflater.*
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.lecture14.R
import com.example.lecture14.activities.models.Mymodel
import kotlinx.android.synthetic.main.card_view.view.*
import com.example.lecture14.activities.adapters.Adapter.Companion.MAIN_ITEM as MAIN_ITEM

class Adapter(var mylist:MutableList<Mymodel>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        const val MAIN_ITEM=1
        const val SECOND_ITEM=2

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if(viewType== MAIN_ITEM){
            return  mainviewholder(from(parent.context).inflate(R.layout.card_view,parent,false))
        }
           return secondviewholder(from(parent.context).inflate(R.layout.second_item,parent,false))
    }

    override fun getItemCount()=mylist.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if(holder is mainviewholder) holder.onbind()
        else if(holder is secondviewholder) holder.onbind()

    }

    inner class mainviewholder(itemView: View): RecyclerView.ViewHolder(itemView) {
       lateinit var model:Mymodel
        fun onbind(){
            model=mylist[adapterPosition]
            itemView.image.setImageResource(model.image)
            itemView.title.text=model.title
            itemView.describe.text=model.desc
        }

    }

    inner class secondviewholder(itemView: View): RecyclerView.ViewHolder(itemView){
        lateinit var model:Mymodel
        fun onbind(){
            model=mylist[adapterPosition]
            itemView.image.setImageResource(model.image)
            itemView.title.text=model.title
            itemView.describe.text=model.desc
        }

    }

    override fun getItemViewType(position: Int): Int {
        if(mylist[position].title=="Kotlin") return SECOND_ITEM
        return MAIN_ITEM

    }
}